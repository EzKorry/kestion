#!/bin/bash

if [ "${TARGET}" == "" ]
then
  TARGET=debug
fi

if [ "${TARGET}" == "release" ]
then
  cargo build --release
  ./target/release/front-server
else
  cargo watch -x run
fi

